import React from "react";
import { TextInput, View, Text, TouchableOpacity, Image } from 'react-native';
import styles from './styles';

export default function Login() {
    return (
        <View style={styles.container}>
            <Image
                style={styles.logoMenuStyle}
                source={require('../assets/alPHa-depositphotos-bgremover.png')}
            />
            <View style={styles.box}>
                <View style={styles.container}>

                    <Text style={styles.titulo}>LOGIN</Text>
                    <TextInput style={styles.text_input}
                        placeholder="usuario"
                        keyboardType="default"
                    />
                    <TextInput style={styles.text_input}
                        placeholder="senha"
                        keyboardType="default"
                    />
                    <TouchableOpacity>
                        <Text style={styles.text}>ACESSAR</Text>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Text style={styles.textSecundary}>Criar conta gratuita</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>

    );
}