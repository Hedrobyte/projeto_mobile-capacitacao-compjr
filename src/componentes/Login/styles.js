import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex:1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },

    box: {
        width: 250,
        height: 250,
        borderRadius: 10,
        backgroundColor: '#DBDBDB',
    },

    logoMenuStyle:{
        width: 300,
        height: 150
    },

    titulo: {
        fontSize: 25,
        fontWeight: 'bold',
    },

    text_input: {
        borderRadius: 5,
        fontSize:20,
        flex:1,
        width: 224,
        margin:5,
        backgroundColor: '#ECECEC'
    },

    text: {
        borderRadius: 50,
        fontWeight: 'bold',
        fontSize:15,
        margin: 5,
        padding: 4,
        color: '#fff',
        backgroundColor: '#6A6A6A'
    },

    textSecundary: {
        fontWeight: 'bold',
        fontSize: 12,
        margin: 5,
        padding: 4,
        color: '#1C1C1C',
    }
})

export default styles;