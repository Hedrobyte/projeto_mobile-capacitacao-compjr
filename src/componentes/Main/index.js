import React from 'react';
import { Image, KeyboardAvoidingView, View } from 'react-native';
import Login from '../Login/';
import styles from './styles';


export default function Main() {
    return (
        <KeyboardAvoidingView style={styles.container}>
            <Login />
        </KeyboardAvoidingView>
    );
}